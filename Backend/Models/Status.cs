namespace Backend.Models{

    public enum Status
    {
        Created,
        Kitchen,
        Packaging,
        Delivery,
        Delivered,
        Canceled
    }
}