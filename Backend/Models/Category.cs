namespace Backend.Models{

    public enum Category
    {
        WOK,
        Pizza,
        Soup,
        Desert,
        Drink
    }
}